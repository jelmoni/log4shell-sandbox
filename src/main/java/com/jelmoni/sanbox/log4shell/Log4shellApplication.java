package com.jelmoni.sanbox.log4shell;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Log4shellApplication {

	public static void main(String[] args) {

        SpringApplication log4shellApplication =
                new SpringApplicationBuilder(
                        Log4shellApplication.class)
                        .properties("spring.config.name:log4shell-application-config")
                        .bannerMode(Banner.Mode.LOG)
                        .banner(new Log4shellApplicationBanner())
                        .build();
        log4shellApplication.run(args);
	}

}
