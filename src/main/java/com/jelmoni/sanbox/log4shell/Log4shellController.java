package com.jelmoni.sanbox.log4shell;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;

@RestController
public class Log4shellController {
    private static final Logger logger = LogManager.getLogger(MethodHandles.lookup()
            .lookupClass());

    @GetMapping("/")
    public String index(@RequestHeader("X-Api-Version") String apiVersion) {
        logger.info("Received a request for API version {}",  apiVersion);
        logger.info("Received a request for API version " + apiVersion);
        return "Log4shell, hello world!";
    }
}
