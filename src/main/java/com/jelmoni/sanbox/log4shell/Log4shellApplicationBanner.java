package com.jelmoni.sanbox.log4shell;

import org.springframework.boot.Banner;
import org.springframework.core.env.Environment;

import java.io.PrintStream;

public class Log4shellApplicationBanner implements Banner {

    @Override
    public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
        System.out.println(" ******************************************");
        System.out.println(" ** log4shell test application           **");
        System.out.println(" ******************************************");
        System.out.println("");
    }
}
